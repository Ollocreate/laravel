@include('templates.header')
<div>
    <h3>{{$article->name}}</h3>
    @if($article->full_image_src)
        <img src="{{$article->full_image_src}}"
    @endif
    <p>{{$article->desc}}</p>
</div>
@include('templates.footer')
