@include('templates.header')
<h1>Все статьи</h1>
<ul class="articles__list">
    @foreach ($articles as $article)
        <li class="articles__item">
            <h3 class="articles__title">{{$article->name}}</h3>
            <p class="articles__text">{{$article->shortDesc}}</p>
            <a href="/news/{{$article->id}}">Читать полностью...</a>
        </li>
    @endforeach
</ul>
@include('templates.footer')
