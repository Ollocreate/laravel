<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
        <link href="/css/style.css" rel="stylesheet">
        <style>
            body {
                font-family: 'Nunito', sans-serif;
            }
        </style>
    </head>
    <body class="antialiased">
        <div class="container">
            <header>
                <a class="logo-link">
                    <img class="logo-img" src="/img/carlogo.svg" alt="">
                </a>
                <ul class="menu">
                    <li class="menu__item">
                        <a href="/" class="menu__link">Главная</a>
                    </li>
                    <li class="menu__item">
                        <a href="/about" class="menu__link">О нас</a>
                    </li>
                    <li class="menu__item">
                        <a href="/news" class="menu__link">Статьи</a>
                    </li>
                </ul>
            </header>
            <main class="content">

