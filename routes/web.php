<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $articles = [
        [   'id'=> 0,
            'title' => 'Article1',
            'text' => 'About Us',
        ],
        [   'id'=> 1,
            'title' => 'Article2',
            'text' => 'About Penguins',
        ],
        [   'id'=> 2,
            'title' => 'Article3',
            'text' => 'About Goats',
        ],
    ];
    return view('main', ['articles'=>$articles]);
});

Route::get('news', [\App\Http\Controllers\ArticleController::class, 'index']);

Route::get('/news/{articleId}', [\App\Http\Controllers\ArticleController::class, 'get']);

Route::get('/about', function () {
    return view('about');
});
